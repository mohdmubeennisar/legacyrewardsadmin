/* eslint-disable comma-dangle */
/* eslint-disable require-jsdoc */
/* eslint-disable object-curly-spacing */
/* eslint-disable indent */
/* eslint-disable max-len */
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

exports.sendNotification = functions.https.onRequest((request, response) => {
  const payload = {
    token:
      "dg4sqzT0RjKz6HbKt9EJnF:APA91bFf0LKNlLWkwTWpVeW17TldSuHadqt_HJB4sO2i5MaBgko_aQD4EwOLB_PoJ_OpOJMc5eWU7OQdIZJQ9CUbYROkpSrPJXxss0o8hur3IZAgijEjK9Jdg7IOb6PuhWgzY68_-IYa",
    notification: {
      title: "This is a title 2",
      body: "This is a body 2",
      sound: "default"
    },
    data: {
      id: "GTbNJtt70hERJeNuhWfh",
    },
  };

  admin
    .messaging()
    .send(payload)
    .then((response) => {
      // Response is a message ID string.
      console.log("Successfully sent message:", response);
      response.send("Notification Sent");
      return { success: true };
    })
    .catch((error) => {
      response.send("Notification Sent");
      return { error: error.code };
    });
});

exports.sendNotificationWithData = functions.https.onRequest(
  (request, response) => {
    // const payload = {
    //   token: request.query.fcmToken,
    //   notification: {
    //     title: request.query.title,
    //     body: request.query.body,
    //     sound: "default",
    //   },
    //   data: {
    //     id: request.query.annoucementId,
    //   },
    // };

    // admin
    //   .messaging()
    //   .send(payload)
    //   .then((response) => {
    //     // Response is a message ID string.
    //     console.log("Successfully sent message:", response);
    //     response.send("Notification Sent");
    //     return { success: true };
    //   })
    //   .catch((error) => {
    //     response.send("Notification Sent");
    //     return { error: error.code };
    //   });
    sendMessage(
      request.query.annoucementId,
      request.query.title,
      request.query.body
    )
      .then((val) => {
        response.status(200).send(val);
      })
      .catch((val) => {
        response.status(500).send(val);
      });
  }
);

async function sendMessage(id, title, body) {
  console.log("send message started");
  // Notification details.
  const payload = {
    notification: {
      id,
      title,
      body,
      sound: "default"
    },
    data: {
      id,
      title,
      body,
    },
  };

  // Get the list of device tokens.
  const allTokens = await admin.firestore().collection("LegacyUsers").get();
  const tokens = [];
  allTokens.forEach((tokenDoc) => {
    const data = tokenDoc.data();
    console.log(data);
    if (data["fcmToken"]) {
      tokens.push(data.fcmToken);
    }
  });

  if (tokens.length > 0) {
    // Send notifications to all tokens.
    await admin.messaging().sendToDevice(tokens, payload);

    console.log("Notifications have been sent and tokens cleaned up.");
  }

  return "OK";
}
