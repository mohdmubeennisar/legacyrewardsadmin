import React, { useState } from "react";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { getFirestore, collection, getDocs } from "firebase/firestore";
import { TextField, Button, makeStyles, Typography } from "@material-ui/core";
import { Link } from "react-router-dom";

import firebase, { db } from "../components/Firebase/firebase";

const useStyles = makeStyles((theme) => ({
  cardContent: {
    backgroundColor: "white",

    maxWidth: 500,
    boxShadow: "5px 10px 40px rgba(120, 120, 120, 0.22)",
    borderRadius: 8,
    display: "block",
    paddingTop: 34,
    paddingBottom: 27,
    paddingLeft: 30,
    paddingRight: 30,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  contentContainerBody: {
    maxWidth: 500,

    margin: "0 auto",
    paddingLeft: 50,
    paddingRight: 50,
    marginTop: 20,

    [theme.breakpoints.down("xs")]: {
      paddingLeft: 20,
      paddingRight: 20,
      marginTop: 40,
    },
  },
  Login: {
    marginTop: "2em",
    width: "18em",
  },
  password: {
    marginTop: "2em",
    width: "18em",
    marginBottom: "2em",
  },
  margin: {
    margin: theme.spacing(1),
    margin: "3em",
  },
  link: {
    marginLeft: "10em",
    textDecoration: "none",
  },
  Button: {
    marginLeft: "1em",
    textDecoration: "none",
  },
  font: {
    fontFamily: "Roboto",
    lineHeight: "1em",
    fontWeight: "500",
  },
  loginButton: {
    marginTop: 20,
    width: 200,
  },
}));
export default function Login() {
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // const handleSubmit

  const signin = async () => {
    const auth = getAuth();

    const adminsCol = collection(db, "Admins");
    const adminSnapshot = await getDocs(adminsCol);

    const adminList = adminSnapshot.docs.map((doc) => doc.data());

    var admin = false;

    adminList.forEach((adminObj) => {
      if (adminObj.email === email) {
        admin = true;
      }
    });

    if (admin) {
      signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          const user = userCredential.user;
          console.log(userCredential);
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          console.log(errorCode);
          console.log(errorMessage);
          alert(errorMessage);
        });
    } else {
      alert("This email does not belong to an admin!");
    }
    // get(child(dbRef, `admins`))
    //   .then((snapshot) => {
    //     if (snapshot.exists()) {
    //       var admins = snapshot.val();
    //       Object.keys(admins).map((key, index) => {
    //         let data = admins[key];
    //         if (data.email === email) {
    //           admin = true;
    //         }
    //       });

    //       if (admin) {
    //         signInWithEmailAndPassword(auth, email, password)
    //           .then((userCredential) => {
    //             const user = userCredential.user;
    //             console.log(userCredential);
    //           })
    //           .catch((error) => {
    //             const errorCode = error.code;
    //             const errorMessage = error.message;
    //             console.log(errorCode);
    //             console.log(errorMessage);
    //             alert(errorMessage);
    //           });
    //       } else {
    //         alert("This email does not belong to an admin!");
    //       }
    //     } else {
    //       alert("This email does not belong to an admin!");
    //     }
    //   })
    //   .catch((error) => {
    //     console.error(error);
    //   });
  };
  return (
    <div className={classes.contentContainerBody}>
      <div className={classes.cardContent}>
        <Typography variant="h3" className={classes.font}>
          Login
        </Typography>
        <TextField
          className={classes.Login}
          id="outlined-basic"
          label="Email"
          variant="outlined"
          type="email"
          onChange={(e) => setEmail(e.target.value)}
        />
        <TextField
          className={classes.password}
          id="outlined-password-input"
          label="Password"
          type="password"
          autoComplete="current-password"
          variant="outlined"
          onChange={(e) => setPassword(e.target.value)}
        />

        <Link className={classes.link} to="/forgot-password">
          Forget Password?
        </Link>

        <Button
          onClick={() => {
            signin();
            //  console.log(signin());
          }}
          variant="contained"
          size="medium"
          className={classes.loginButton}
        >
          Login
        </Button>
      </div>
    </div>
  );
}
