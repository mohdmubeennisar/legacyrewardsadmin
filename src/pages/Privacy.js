import React, { useState } from "react";

import { TextField, Button, makeStyles, Typography } from "@material-ui/core";
import { Link } from "react-router-dom";

import firebase, { db } from "../components/Firebase/firebase";

const useStyles = makeStyles((theme) => ({
  cardContent: {
    backgroundColor: "white",

    maxWidth: 500,
    boxShadow: "5px 10px 40px rgba(120, 120, 120, 0.22)",
    borderRadius: 8,
    display: "block",
    paddingTop: 34,
    paddingBottom: 27,
    paddingLeft: 30,
    paddingRight: 30,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  contentContainerBody: {
    maxWidth: 1280,

    margin: "0 auto",
    paddingLeft: 50,
    paddingRight: 50,
    marginTop: 20,
    paddingBottom: 200,

    [theme.breakpoints.down("xs")]: {
      paddingLeft: 20,
      paddingRight: 20,
      marginTop: 40,
      paddingBottom: 50,
    },
  },
  Login: {
    marginTop: "2em",
    width: "18em",
  },
  password: {
    marginTop: "2em",
    width: "18em",
    marginBottom: "2em",
  },
  margin: {
    margin: theme.spacing(1),
    margin: "3em",
  },
  link: {
    marginLeft: "10em",
    textDecoration: "none",
  },
  Button: {
    marginLeft: "1em",
    textDecoration: "none",
  },
  font: {
    fontFamily: "Roboto",
    lineHeight: "1em",
    fontWeight: "500",
    marginTop: 20,
  },
  loginButton: {
    marginTop: 20,
    width: 200,
  },
}));
export default function Privacy() {
  const classes = useStyles();

  return (
    <div className={classes.contentContainerBody}>
      <Typography variant="h4" className={classes.font}>
        Privacy Policy & Terms of use
      </Typography>
      <Typography variant="h6" className={classes.font}>
        Legacy Rewards
      </Typography>
      <Typography variant="h6" className={classes.font}>
        Effective date: 1st June 2021
      </Typography>
      <Typography variant="h6" className={classes.font}>
        Welcome to Legacy Rewards (the “Site”) which is owned by Quintero &
        Partners (“we”, “us” and “our”) and operated from the United States.
        This privacy policy has been compiled to better serve those who are
        concerned with how their ‘Personally Identifiable Information’ (PII) is
        being used online. PII, as described in US privacy law and information
        security, is information that can be used on its own or with other
        information to identify, contact, or locate a single person, or to
        identify an individual in context. Please read our privacy policy
        carefully to get a clear understanding of how we collect, use, protect,
        or otherwise handle your PII in accordance with our website.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        Collection of Personal and Non-Personal Information (PII and Non-PII)
      </Typography>
      <Typography variant="h6" className={classes.font}>
        We collect and store certain personally identifiable information about
        you (i.e., information which may identify you in some way; such as your
        name, resumes/CVs email address, screen name and/or financial
        information) (“PII”) only when you voluntarily submit it.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        We collect and maintain different types of personal information in
        respect of those individuals who seek to be, are, or were considered to
        be members of the discounts program.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        When you connect to our Site, there is certain technical, non-personally
        identifiable information which does not identify any individual
        (“Non-PII”) that may be collected and stored through the use of “cookie”
        technology and IP addresses. PII is not extracted in this process.
        Non-PII may include, but is not limited to, your IP host address,
        internet browser type, personal computer settings, and the number,
        duration, and character of page visits. This information assists us to
        update a Site so that it remains interesting to our visitors and
        contains content that is tailored to visitors’ interests.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        Use of Your PII. When you provide your PII at a Site, we will limit the
        use of the PII for the purpose for which it was collected in accordance
        with the terms of this Privacy Policy. Other limited uses of your PII
        may include:
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --Determining eligibility for initial employment, including the
        verification of references and qualifications.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --To respond to your questions, comments, and requests; to provide you
        with access to certain areas and features on a Site; and to communicate
        with you about your activities on a Site.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --To investigate suspected fraud, harassment, physical threats, or other
        violations of any law, rule or regulation, the rules or policies of a
        Site, or the rights of third parties; or to investigate any suspected
        conduct which we deem improper.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --To the extent that you provide us with financial information in
        connection with shopping or commercial services offered on a Site, we
        may use the financial information that you provide to fulfill the
        services.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --To help us develop, deliver and improve our products, services,
        content and advertising.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --To share with our parent, subsidiary, and affiliated companies, and
        promotional partners involved in creating, producing, delivering, or
        maintaining a Site, as required to perform functions on our behalf in
        connection with the Site (such as administration of the Site,
        administration of promotions or other features on the Site, marketing,
        data analysis, and customer services). In order to share such
        information, it may be necessary for us to transmit your PII outside the
        jurisdiction set forth below and you agree to this transfer. Further use
        or disclosure of PII by these parties for other purposes is not
        permitted.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --To share with third party service providers whom we employ to perform
        functions on our behalf in connection with the Site (such functions may
        include, but are not limited to, sending postal mail and e-mail,
        removing repetitive information from customer lists, analyzing data and
        providing marketing assistance, and providing customer service). These
        third party service providers have access to PII that is needed to
        perform their functions, and may collect or store PII as part of their
        performance of these functions, but are not authorized to use it for
        other purposes.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --For our internal purposes such as auditing, data analysis, and
        research to improve our products, services and customer communications.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --For other purposes as disclosed when your PII is collected or in any
        additional terms and conditions applicable to a particular feature of a
        Site.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --For disclosures required by law, regulation, or court order.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --For the purpose of or in connection with legal proceedings or
        necessary for establishing, defending, or exercising legal rights.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --In an emergency to protect the health or safety of users of a Site or
        the general public, or in the interests of national security.
      </Typography>

      <Typography variant="h6" className={classes.font}>
        Consent
      </Typography>

      <Typography variant="h6" className={classes.font}>
        You consent and authorize Quintero & Partners to the use of manual or
        automatic technology to call you, send you text messages, or email you
        to the number provided above, including your mobile number.
      </Typography>

      <Typography variant="h6" className={classes.font}>
        Except as provided for herein, we will not provide any of your PII to
        any third parties without your specific consent.
      </Typography>

      <Typography variant="h6" className={classes.font}>
        Managing Your Personal Information. You have the ultimate control over
        the PII that we collect and use. You can always chose not to provide
        certain PII, but please keep in mind that you may not be able to use
        some of the features offered by a Site unless you provide us with your
        PII. If you wish to verify, update, or correct any of your PII collected
        through a Site, contact us on the details provided in the contact
        section of this policy.
      </Typography>

      <Typography variant="h6" className={classes.font}>
        We may also use your PII for the following purposes:
      </Typography>

      <Typography variant="h6" className={classes.font}>
        Direct Marketing. We may use your personal information to send you
        Quintero & Partners-related marketing communications as permitted by
        law. You will have the ability to opt-out of our marketing and
        promotional communications as described in the Your Choices section
        below.
      </Typography>

      <Typography variant="h6" className={classes.font}>
        For research and development. We may use your personal information for
        research and development purposes, including to analyze and improve the
        Service and our business.
      </Typography>

      <Typography variant="h6" className={classes.font}>
        To create anonymous data. We may create aggregated, de-identified or
        other anonymous data records from your personal information and other
        individuals whose personal information we collect. We make personal
        information into anonymous data by excluding information (such as your
        name) that makes the data personally identifiable to you. We may use
        this anonymous data and share it with third parties for our lawful
        business purposes, including to analyze and improve the Service and
        promote our business.
      </Typography>

      <Typography variant="h6" className={classes.font}>
        To comply with laws and regulations. We use your personal information as
        we believe necessary or appropriate to comply with applicable laws,
        lawful requests, and legal process, such as to respond to subpoenas or
        requests from government authorities.
      </Typography>

      <Typography variant="h6" className={classes.font}>
        For compliance, fraud prevention and safety. We may use your personal
        information and disclose it to law enforcement, government authorities,
        and private parties as we believe necessary or appropriate to: (a)
        protect our, your or others’ rights, privacy, safety or property
        (including by making and defending legal claims); (b) audit our internal
        processes for compliance with legal and contractual requirements; (c)
        enforce the terms and conditions that govern the Service; and (d)
        protect, investigate and deter against fraudulent, harmful,
        unauthorized, unethical or illegal activity, including cyber-attacks and
        identity theft.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        With your consent. In some cases we may specifically ask for your
        consent to collect, use or share your personal information, such as when
        required by law.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        Cookies
      </Typography>
      <Typography variant="h6" className={classes.font}>
        Please refer to our cookie policy to know more about how we handle
        cookies.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        Security
      </Typography>
      <Typography variant="h6" className={classes.font}>
        We adopt appropriate data collection, storage and processing practices
        and security measures to protect against unauthorized access,
        alteration, disclosure or destruction of your personal information,
        username, password, transaction information and data stored on our Site.
        The security on the internet is not 100% guaranteed, however we use best
        efforts to keep it secure for the users. We do not accept liability for
        unintentional disclosure. We encourage you to use caution when using the
        Internet.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        CCPA
      </Typography>
      <Typography variant="h6" className={classes.font}>
        We are required by the California Consumer Privacy Act (“CCPA”) to
        provide to California residents an explanation of how we collect, use
        and share their Personal Information, and of the rights and choices, we
        offer to California residents concerning that Personal Information.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        Personal information that we collect, use, and share. We do not sell
        personal information. As we explain in this Privacy Policy, we use
        cookies and other tracking tools to analyze website traffic and
        facilitate advertising.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        Your California privacy rights. The CCPA grants California residents the
        following rights. However, these rights are not absolute, and in certain
        cases we may decline your request as permitted by law.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --Information. You can request information about how we have collected,
        used and shared and used your Personal Information during the past 12
        months.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        ----The categories of Personal Information that we have collected.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        ----The categories of sources from which we collected Personal
        Information.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        ----The business or commercial purpose for collecting and/or selling
        Personal Information.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        ----The categories of third parties with whom we share Personal
        Information.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        ----Whether we have disclosed your Personal Information for a business
        purpose, and if so, the categories of Personal Information received by
        each category of third party recipient.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        ----Whether we’ve sold your Personal Information, and if so, the
        categories of Personal Information received by each category of third
        party recipient.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --Information. You can request information about how we have collected,
        used and shared and used your Personal Information during the past 12
        months.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --Access. You can request a copy of the Personal Information that we
        have collected about you during the past 12 months.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --Deletion. You can ask us to delete the Personal Information that we
        have collected from you.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        You are entitled to exercise the rights described above free from
        discrimination in the form of legally prohibited increases in the price
        or decreases in the quality of our Service.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        How to exercise your California rights. You may exercise your California
        privacy rights described above as follows:
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --Right to information, access and deletion. You can request to exercise
        your information, access and deletion rights, by emailing
        Contact@Quinteropartners.com. We reserve the right to confirm your
        California residence to process your requests and will need to confirm
        your identity to process your requests to exercise your information,
        access or deletion rights. As part of this process, government
        identification may be required. Consistent with California law, you may
        designate an authorized agent to make a request on your behalf. In order
        to designate an authorized agent to make a request on your behalf, you
        must provide a valid power of attorney, the requester’s valid
        government-issued identification, and the authorized agent’s valid
        government issued identification. We cannot process your request if you
        do not provide us with sufficient detail to allow us to understand and
        respond to it.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        --Request a list of third party marketers. California’s “Shine the
        Light” law (California Civil Code § 1798.83) allows California residents
        to ask companies with whom they have formed a business relationship
        primarily for personal, family or household purposes to provide certain
        information about the companies’ sharing of certain personal information
        with third parties for their direct marketing purposes during the
        preceding year (if any). You can submit such a request by sending an
        email to Contact@Quinteropartners.com with “Shine the Light” in the
        subject line. The request must include your current name, street
        address, city, state, and zip code and attest to the fact that you are a
        California resident.
      </Typography>
      <Typography variant="h6" className={classes.font}>
        We cannot process your request if you do not provide us with sufficient
        detail to allow us to understand and respond to it.
      </Typography>
    </div>
  );
}
