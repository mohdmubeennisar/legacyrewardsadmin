import { useState, useEffect, useContext } from "react";
// import { useNavigate } from 'react-router-dom';
// material-ui
import { useTheme } from "@material-ui/core/styles";
import {
  Avatar,
  Button,
  Grid,
  InputAdornment,
  Menu,
  MenuItem,
  OutlinedInput,
  Pagination,
  Typography,
  TextField,
  ButtonBase,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { Context as AuthContext } from "../context/AuthContext";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { db, storage } from "../components/Firebase/firebase";
import { addDoc, collection } from "firebase/firestore";
// project imports

// ==============================|| USER LIST STYLE 2 ||============================== //

const AddAnnoucement = () => {
  const theme = useTheme();
  const [anchorEl, setAnchorEl] = useState(null);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [urlAnn, setUrl] = useState("");
  const [haveMedia, setHaveMedia] = useState(false);
  const [loading, setLoading] = useState(false);

  const [mediaFileSelector, setMediaFileSelector] = useState(null);

  const [mediaFile, setMediaFile] = useState(null);
  const [mediaSrc, setMediaSrc] = useState(null);

  // const navigate = useNavigate();

  const {
    state: { user },
    setuser,
  } = useContext(AuthContext);

  

  const buildFileSelector = () => {
    const fileSelector = document.createElement("input");
    fileSelector.setAttribute("type", "file");
    fileSelector.setAttribute("accept", "image/jpeg,image/png");
    return fileSelector;
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const submit = async () => {
    if (!title) {
      return alert("Please enter a title");
    }

    if (!description) {
      return alert("Please enter a description");
    }

     if (!urlAnn) {
      return alert("Please enter a url");
    }

    if (!mediaFile) {
      return alert("Please choose a file to upload");
    }
    setLoading(true);
    const storageRef = ref(storage, `/annoucements/${mediaFile.name}`);
    const uploadTask = uploadBytesResumable(storageRef, mediaFile);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const percent = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        // console.log("percent", percent);
      },
      (err) => {
        console.log(err);
        setLoading(false);
      },
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((url) => {
          console.log("heeee");

          const usersCol = collection(db, "Announcements");
          addDoc(usersCol, {
            title,
            image: url,
            body: description,
            createdAt: new Date().toISOString(),
            url: urlAnn
          })
            .then(() => {
              // console.log("Added data");
              alert('Annoucement Added!');
              setTitle("");
              setDescription("");
              setUrl("");
              setMediaFile("");
              setLoading(false);
            })
            .catch((e) => {
              setLoading(false);
              // console.log("E", e);
            });
        });
      }
    );

    //createGamingPost(postText, user.id, mediaFile);
  };

  useEffect(() => {
    const mFileSelector = buildFileSelector();
    mFileSelector.addEventListener("input", (event) => {
      const file = mFileSelector.files[0];
      console.log(file);
      if (file.type.includes("image")) {
        const reader = new FileReader();
        const url = reader.readAsDataURL(file);
        reader.onloadend = function (e) {
          setMediaSrc(reader.result);
        };
      }

      setMediaFile(file);
      // setHaveMedia(true);
    });

    setMediaFileSelector(mFileSelector);
  }, []);

  return (
    <div style={{ padding: 10 }}>
      <Grid
        container
        justifyContent="space-between"
        alignItems="center"
        spacing={1}
      >
        <Grid item>
          <Typography variant="h3">Add Annoucment</Typography>
        </Grid>
      </Grid>
      <Grid item md={6} xs={12}>
        <TextField
          fullWidth
          id="outlined-multiline-flexible"
          label="Title"
          variant="outlined"
          value={title}
          onChange={(event) => {
            setTitle(event.target.value);
          }}
        />
      </Grid>
      <Grid item md={6} xs={12}>
        <TextField
          style={{ marginTop: 20 }}
          fullWidth
          id="outlined-multiline-flexible"
          label="Description"
          variant="outlined"
          multiline
          rows={3}
          value={description}
          onChange={(event) => {
            setDescription(event.target.value);
          }}
        />
      </Grid>
      <Grid item md={6} xs={12}>
        <TextField
          fullWidth
           style={{ marginTop: 20 }}
          id="outlined-multiline-flexible"
          label="Url"
          variant="outlined"
          value={urlAnn}
          onChange={(event) => {
            setUrl(event.target.value);
          }}
        />
      </Grid>
      <Grid item md={6} xs={12}>
        <Typography variant="h6" style={{ marginTop: 20 }}>
          Annoucment Media
        </Typography>
        {mediaFile && mediaFile.type.includes("image") && (
          <img
            src={mediaSrc}
            alt="media"
            style={{ width: 300, height: 300, objectFit: "contain" }}
          />
        )}
        {mediaFile && (
          <Typography style={{ marginTop: 10 }}>{mediaFile.name}</Typography>
        )}
        <ButtonBase sx={{ borderRadius: "12px" }}>
          <Avatar
            variant="rounded"
            sx={{
              ...theme.typography.commonAvatar,
              ...theme.typography.mediumAvatar,
              transition: "all .2s ease-in-out",
              background:
                theme.palette.mode === "dark"
                  ? theme.palette.dark.main
                  : theme.palette.secondary.light,
              color:
                theme.palette.mode === "dark"
                  ? theme.palette.warning.dark
                  : theme.palette.secondary.dark,
              '&[aria-controls="menu-list-grow"],&:hover': {
                background:
                  theme.palette.mode === "dark"
                    ? theme.palette.warning.dark
                    : theme.palette.secondary.dark,
                color:
                  theme.palette.mode === "dark"
                    ? theme.palette.grey[800]
                    : theme.palette.secondary.light,
              },
            }}
            onClick={() => {
              mediaFileSelector.click();
            }}
          >
            <AddIcon stroke={1.5} size="1.3rem" />
          </Avatar>
        </ButtonBase>
      </Grid>
      <Grid item lg={2} md={4} sm={6} xs={6} style={{ marginTop: 20 }}>
        <Button
          onClick={() => {
            submit();
          }}
          disabled={loading}
          variant="outlined"
          fullWidth
          size="small"
          sx={{ minWidth: 120 }}
          startIcon={<AddIcon />}
        >
          {!loading ? "Add" : "Adding..."}
        </Button>
      </Grid>
    </div>
  );
};

export default AddAnnoucement;
