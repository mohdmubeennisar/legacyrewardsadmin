import React, { useState } from "react";
import {
  TextField,
  Button,
  makeStyles,
  Typography,
  Link,
} from "@material-ui/core";

import { getAuth, sendPasswordResetEmail } from "firebase/auth";

const useStyles = makeStyles((theme) => ({
  cardContent: {
    backgroundColor: "white",
    maxWidth: 400,
    boxShadow: "5px 10px 40px rgba(120, 120, 120, 0.22)",
    borderRadius: 8,
    display: "block",
    paddingTop: 34,
    paddingBottom: 27,
    paddingLeft: 30,
    paddingRight: 30,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  contentContainerBody: {
    maxWidth: 500,

    margin: "0 auto",
    paddingLeft: 50,
    paddingRight: 50,
    marginTop: 20,

    [theme.breakpoints.down("xs")]: {
      paddingLeft: 20,
      paddingRight: 20,
      marginTop: 40,
    },
  },
  Login: {
    marginTop: "5em",
    width: "22em",
  },
  password: {
    marginTop: "2em",
    width: "18em",
  },
  margin: {
    margin: theme.spacing(1),
    margin: "3em",
  },
  link: {
    marginLeft: "12em",
  },
  font: {
    fontFamily: "Roboto",
    lineHeight: "1em",
    fontWeight: "500",
  },
  forgotButton: {
    marginTop: 20,
    width: 200,
  },
}));
export default function ForgotPassword() {
  const classes = useStyles();
  const [email, setEmail] = useState("");

  const forgotPassword = () => {
    const auth = getAuth();
    sendPasswordResetEmail(auth, email)
      .then((success) => {
        alert("Please check your email for the reset link!");
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(errorCode);
        console.log(errorMessage);
        alert(errorMessage);
      });
  };

  return (
    <div className={classes.contentContainerBody}>
      <div className={classes.cardContent}>
        <Typography variant="h3" className={classes.font}>
          Reset Password
        </Typography>
        <TextField
          className={classes.Login}
          id="outlined-basic"
          label="Email Address"
          variant="outlined"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Button
          variant="contained"
          size="medium"
          className={classes.forgotButton}
          onClick={() => {
            forgotPassword();
          }}
        >
          Reset
        </Button>
      </div>
    </div>
  );
}
