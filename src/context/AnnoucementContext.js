import createDataContext from "./createDataContext";
import firebase, { db } from "../components/Firebase/firebase";
import {
  getFirestore,
  collection,
  getDocs,
  deleteDoc,
  doc,
} from "firebase/firestore";

import axios from "axios";

const annoucementReducer = (state, action) => {
  switch (action.type) {
    case "set_annoucement":
      return { ...state, annoucements: action.payload };
    case "set_loading":
      return { ...state, loading: action.payload };
    case "delete_annoucement":
      let id = action.payload;
      let anns = state.annoucements;
      let i = -1;
      anns.forEach((a, index) => {
        if (a.id === id) {
          i = index;
        }
      });
      if (i !== -1) {
        anns.splice(i, 1);
      }
      return { ...state, annoucements: anns };
    default:
      return state;
  }
};

const setAnnoucements = (dispatch) => async (annoucements) => {
  dispatch({ type: "set_annoucement", payload: annoucements });
};

const setLoading = (dispatch) => async (loading) => {
  dispatch({ type: "set_loading", payload: loading });
};

const fetchAnnoucements = (dispatch) => async () => {
  dispatch({ type: "set_loading", payload: true });

  const annoucementsCol = collection(db, "Announcements");
  const annoucementSnapshot = await getDocs(annoucementsCol);

  const annoucementList = annoucementSnapshot.docs.map((doc) => {
    let d = {
      id: doc.id,
      ...doc.data(),
    };
    return d;
  });
  dispatch({ type: "set_annoucement", payload: annoucementList });
  dispatch({ type: "set_loading", payload: false });
};

const deleteAnnoucement = (dispatch) => async (item) => {
  dispatch({ type: "set_loading", payload: true });

  const annoucementDocRef = doc(db, "Announcements", item);

  await deleteDoc(annoucementDocRef);
  dispatch({ type: "delete_annoucement", payload: item });

  dispatch({ type: "set_loading", payload: false });
};

const sendNotification = (dispatch) => async (item) => {
  dispatch({ type: "set_loading", payload: true });

  axios.get(
    "https://us-central1-legacy-rewards.cloudfunctions.net/sendNotificationWithData",
    {
      params: {
        // fcmToken:
        //   "eSq7RTjgR00HjPa8CPubV3:APA91bGBwhd2q4qtMIqWvXbpEclXPWwwaSkVJmJiwIKEmzfjYFV5uKctYcLod3cllLsKdxb3DLLT_pIPAg-PaQYn8xPQIUJJbXXVopZNvI3BqgfcPcA4L15h_GUl8-p1mZ9A8aYZF5d3",
        title: item.title,
        body: item.body,
        annoucementId: item.id,
      },
    }
  );

  alert("Notification sent!");
  dispatch({ type: "set_loading", payload: false });
};

export const { Provider, Context } = createDataContext(
  annoucementReducer,
  {
    setAnnoucements,
    setLoading,
    fetchAnnoucements,
    deleteAnnoucement,
    sendNotification,
  },
  { annoucements: null, loading: false }
);
