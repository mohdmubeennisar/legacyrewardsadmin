import React from "react";
import firebase from "./components/Firebase/firebase";
import Router from "./components/Router";

import { Provider as AuthProvider } from "./context/AuthContext";
import { Provider as AnnoucmentProvider } from "./context/AnnoucementContext";

function App() {
  return <AnnoucmentProvider>
    <AuthProvider>
      <Router />
    </AuthProvider>
    </AnnoucmentProvider>
}

export default App;
