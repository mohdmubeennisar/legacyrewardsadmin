import React, { useContext, useState, useEffect } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { Context as AuthContext } from "../context/AuthContext";

import Login from "../pages/Login";
import ForgotPassword from "../pages/ForgotPassword";
import Header from "./Header";
import Privacy from "../pages/Privacy";
import Users from "../pages/Users";
import Annoucements from "../pages/Annoucements";
import AddAnnoucement from "../pages/AddAnnoucement";
import EditAnnoucement from "../pages/EditAnnoucement";

function Router() {
  const [loading, setLoading] = useState(false);
  const {
    state: { user },
    setuser,
  } = React.useContext(AuthContext);

  console.log(user);

  const auth = getAuth();

  useEffect(() => {
    
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        setuser(user);
      } else {
        setuser(null);
      }
    });
    return () => {
      unsubscribe();
    };
  }, []);

  const PrivateRoute = ({ children, ...rest }) => {
    
    return (
      <Route
        {...rest}
        render={({ location }) =>
          user ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: location },
              }}
            />
          )
        }
      />
    );
  };

  if (loading) {
    return (
      <div style={{ height: 800 }}>
        <h3></h3>
      </div>
    );
  }

  return (
    <BrowserRouter>
      {user && (
<Header />
      )}
      
      <Switch>
        <Route
          exact
          path="/"
          render={() => {
            return user ? <Redirect to="/users" /> : <Login />;
          }}
        />
        <Route
          exact
          path="/forgot-password"
          render={() => {
            return user ? <Redirect to="/users" /> : <ForgotPassword />;
          }}
        />
        <PrivateRoute path="/users">
          <Users />
        </PrivateRoute>
          <PrivateRoute path="/annoucements">
          <Annoucements />
        </PrivateRoute>

         <PrivateRoute path="/add-annoucment">
          <AddAnnoucement />
        </PrivateRoute>
         <PrivateRoute path="/edit-annoucment/:id">
          <EditAnnoucement />
        </PrivateRoute>
      </Switch>
    </BrowserRouter>
  );
}

export default Router;
