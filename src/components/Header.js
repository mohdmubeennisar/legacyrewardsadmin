import React, { useState } from "react";
import {
  TextField,
  Button,
  makeStyles,
  Typography,
  
} from "@material-ui/core";
import { Link, useHistory } from "react-router-dom";

import { getAuth, sendPasswordResetEmail,signOut } from "firebase/auth";

const useStyles = makeStyles((theme) => ({
  headerContainer: {
    display: "flex",
    width: "100%",
    justifyContent: "flex-end",
    alignItems: "center",
    
  },
  logo: {
    width: 200,
    height: 200,
    objectFit: "contain",
  },
   loginButton: {
    margin: 20,
  },
  navBar: {
    flex: 1,
    marginLeft:20,
    flexDirection: 'row',
   display:'flex'
  },
  navElement: {
    color: 'black',
    textDecorationLine: 'underline',
    marginRight:20
  }
}));
export default function Header() {
  const classes = useStyles();

   const signout = () => {
    const auth = getAuth();
    signOut(auth);
  };

  return <div className={classes.headerContainer}>
    <div className={classes.navBar}>
       <Link to="/users" style={{ textDecoration: "none" }}>
                  <Typography
                   
                    className={classes.navElement}
                  >
                   Users
                  </Typography>
      </Link>
      
       <Link to="/annoucements" style={{ textDecoration: "none" }}>
                  <Typography
                    className={classes.navElement}
                    
                  >
                   Annoucements
                  </Typography>
                </Link>
       
    </div>
     <Button
        onClick={() => {
          signout();
          //  console.log(signin());
        }}
        variant="contained"
        size="medium"
        className={classes.loginButton}
      >
        Logout
    </Button>
    
    
  </div>;
}
