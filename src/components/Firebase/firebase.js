import { initializeApp } from "firebase/app";
import { getFirestore, collection, getDocs } from "firebase/firestore";
import { getStorage } from "firebase/storage";
const firebaseConfig = {
  apiKey: "AIzaSyCfOc_ck9MCIS3nNvpeYjF_Bu1ic9K2dFk",
  authDomain: "legacy-rewards.firebaseapp.com",
  projectId: "legacy-rewards",
  storageBucket: "legacy-rewards.appspot.com",
  messagingSenderId: "111281147454",
  appId: "1:111281147454:web:c611f26142395118c9cb6f",
};
const firebase = initializeApp(firebaseConfig);
export default firebase;

export const db = getFirestore(firebase);
export const storage = getStorage(firebase);
